<?php

namespace App\Http\Middleware;
use Auth;

use Closure;
use Illuminate\Http\Request;

class RoleMIddleware
{
    public function handle(Request $request, Closure $next)
    {
        session(['role' => Auth::user()->role->name]); //cetak session
        return $next($request);
    }
}
