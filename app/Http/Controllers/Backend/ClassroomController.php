<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Kelas;
use App\Models\Level;
use App\Models\Hari;
use App\Models\Tahun;
use App\Models\Mapel;
use App\Models\Jadwal;

class ClassroomController extends Controller
{
    public function index()
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $haris = Hari::all();
            return view('backend/dir_classroom/index', compact('haris'));
        }else{
            return back();
        }
        
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($slug_hari)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $tahun_ajar = Tahun::where('is_active', 1)->first();
            $jadwal_s = Jadwal::where('tahun_id', $tahun_ajar->id)->where('user_id', Auth::user()->id)->where('hari_id', $hari->id)->orderBy('jam_mulai','asc')->get();
            return view('backend/dir_classroom/indexKelas', compact('hari', 'jadwal_s'));
        }else if(session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $tahun_ajar = Tahun::where('is_active', 1)->first();
            if(Auth::user()->kelas_s()->where('is_active', 1)->exists()){
                $kelas = Auth::user()->kelas_s()->where('is_active', 1)->first();
                $jadwal_s = Jadwal::where('tahun_id', $tahun_ajar->id)->where('kelas_id', $kelas->id)->where('hari_id', $hari->id)->orderBy('jam_mulai','asc')->get();
                return view('backend/dir_classroom/indexKelas', compact('hari', 'jadwal_s'));
            }
            return back();
        }else{
            return back();
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
