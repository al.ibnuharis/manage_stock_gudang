<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon;
use App\Models\Materi;
use App\Models\Hari;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Ebook;
use App\Models\Soal;
use App\Models\Segment;
use App\Models\TipePertanyaan;
use App\Models\Pertanyaan;

class SettingSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            return view('backend/dir_materi/indexSettingSoal', compact('hari', 'materi', 'mapel', 'kelas', 'ebooks', 'soal'));
        }else{
            return back();
        }
    }

    // 
    public function storeSegmentSoal(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $segment = Segment::create($request->all());
            if($segment->soals()->where('soal_id', $soal_id )->exists()){
                dd('sudah ada');
            }
            $segment->soals()->attach($soal_id);
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    public function updateSegmentSoal(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $segment = Segment::find($segment_id);
            $segment->update($request->all());
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    
    public function destroySegmentSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $segment = Segment::find($segment_id);
            $segment->delete($segment);
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }
    
    public function copotSegmentSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $soal->segments()->wherePivot('segment_id', $segment_id)->detach();
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }
    
    public function pasangSegmentSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            if($soal->segments()->where('segment_id', $segment_id )->exists()){
                return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
            }  
            $soal->segments()->attach($segment_id);
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }
    
    public function indexSegmentSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            $segment = Segment::find($segment_id);
            $tipe_pertanyaans = TipePertanyaan::all();
            return view('backend/dir_materi/indexSegment', compact('tipe_pertanyaans', 'segment', 'hari', 'materi', 'mapel', 'kelas', 'ebooks', 'soal'));
        }else{
            return back();
        }
    }
    
    public function storePertanyaan(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id)
    {
        if(session('role') === 'Guru'){
            $pertanyaan = Pertanyaan::create($request->all());
            $segment = Segment::find($segment_id);
            if($segment->pertanyaans()->where('pertanyaan_id', $pertanyaan->id )->exists()){
                return redirect()->route('indexSegmentSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id])->with('done', 'successfully created !!');
            }
            $segment->pertanyaans()->attach($pertanyaan->id);
            return redirect()->route('indexSegmentSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    public function updatePertanyaan(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id, $pertanyaan_id)
    {
        if(session('role') === 'Guru'){
            $pertanyaan = Pertanyaan::find($pertanyaan_id);
            $pertanyaan->update($request->all());
            return redirect()->route('indexSegmentSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }
    
    public function destroyPertanyaan($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id, $pertanyaan_id)
    {
        if(session('role') === 'Guru'){
            $pertanyaan = Pertanyaan::find($pertanyaan_id);
            $pertanyaan->delete($pertanyaan);
            return redirect()->route('indexSegmentSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id, $segment_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }
    

    public function createKunciSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            $for_as = 'create_kunci';
            return view('backend/dir_materi/pertanyaan', compact('for_as', 'ebooks', 'hari', 'materi', 'mapel', 'kelas', 'soal'));
        }else{
            return back();
        }
    }
    
    public function kerjakanSoal($slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Siswa'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            $for_as = 'kerjakan_soal';
            return view('backend/dir_materi/pertanyaan', compact('for_as', 'ebooks', 'hari', 'materi', 'mapel', 'kelas', 'soal'));
        }else{
            return back();
        }
    }
    

    public function storeKunciSoal(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Guru'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            $question_id = array_keys($request->except('_token'));
            $jawaban =  array_values($request->except('_token'));
            $loop = count($request->except('_token'));
            for($i = 0; $i < $loop; $i++){ 
                if(Pertanyaan::where('id', $question_id[$i])->exists()){
                    $question = Pertanyaan::where('id', $question_id[$i])->first();
                    if(is_array($jawaban[$i])){
                        $answer = '';
                        for($l = 0; $l < count($jawaban[$i]); $l++){
                            if($l == count($jawaban[$i])-1){
                                $answer .=$jawaban[$i][$l];
                            }else{
                                $answer .=$jawaban[$i][$l].'~';
                            }
                        }
                    }else{
                        $answer =$jawaban[$i];
                    }
                    $question->jawaban = $answer;
                    $question->save();
                }
            }
            return redirect()->route('indexSettingSoal', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
        return back();
    }

    public function storeKerjakanSoal(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $soal_id)
    {
        if(session('role') === 'Siswa'){
            $soal = Soal::find($soal_id);
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            $question_id = array_keys($request->except('_token'));
            $jawaban =  array_values($request->except('_token'));
            $loop = count($request->except('_token'));
            $true = 0;
            for($i = 0; $i < $loop; $i++){
                if(is_array($jawaban[$i])){
                    $answer = '';
                    for($l = 0; $l < count($jawaban[$i]); $l++){
                        if($l == count($jawaban[$i])-1){
                            $answer .=$jawaban[$i][$l];
                        }else{
                            $answer .=$jawaban[$i][$l].'~';
                        }
                    }
                }else{
                    $answer =$jawaban[$i];
                }
                // dd($answer);

                if(Pertanyaan::where('id', $question_id[$i])->exists()){
                    $question = Pertanyaan::where('id', $question_id[$i])->first();
                    // dd($question);
                    if($question->jawaban == $answer){
                        DB::table('jawab_soals')->insert([
                            'user_id' => Auth::user()->id,
                            'soal_id' => $soal->id,
                            'pertanyaan_id' => $question_id[$i],
                            'jawaban' => $answer,
                            'created_at' =>  \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now(),
                        ]);
                        $true = $true+1;
                    }else{
                        DB::table('jawab_soals')->insert([
                            'user_id' => Auth::user()->id,
                            'soal_id' => $soal->id,
                            'pertanyaan_id' => $question_id[$i],
                            'jawaban' => $answer,
                            'created_at' =>  \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now(),
                        ]);
                        $true = $true+0;
                    }
                }else{
                    DB::table('jawab_soals')->insert([
                        'user_id' => Auth::user()->id,
                        'soal_id' => $soal->id,
                        'pertanyaan_id' => $question_id[$i],
                        'jawaban' => $answer,
                        'created_at' =>  \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ]);
                    $true = $true+0;
                }
            }
            $nilai = ceil(($true / $loop) * 100);

            if($soal->user_s()->where('user_id', Auth::user()->id)->exists()){
                return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
            }else{
                $soal->user_s()->attach(Auth::user()->id, ['nilai' => $nilai]);
                $soal->save();
                return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
            } // cetak hasil
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
