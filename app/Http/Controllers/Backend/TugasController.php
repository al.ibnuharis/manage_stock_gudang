<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon;
use App\Models\Materi;
use App\Models\Hari;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Ebook;
use App\Models\Tugas;

class TugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $tugas = Tugas::find($tugas_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            return view('backend/dir_materi/indexTugas', compact('tugas', 'hari', 'materi', 'mapel', 'kelas', 'ebooks'));
        }else{
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::create($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Tugas_' . $tugas->id . ' ' . $filename;
                $path = Storage::putFileAs('public/tugas_s', $request->file('file'), $newName);
                $tugas->file_path = $newName;
            }
            $tugas->save();
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $tugas = Tugas::find($tugas_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            return view('backend/dir_materi/showTugas', compact('tugas', 'hari', 'materi', 'mapel', 'kelas', 'ebooks'));
        }else{
            return back();
        }
    }

    public function download($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $tugas = Tugas::find($tugas_id);
            return Storage::download('public/tugas_s/'.$tugas->file_path);
        }else{
            return back();
        }
    }

    public function downloadTugasSiswa($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id, $siswa_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::find($tugas_id);
            $file = $tugas->user_s()->where('user_id', $siswa_id)->first()->pivot->file_path;
            return Storage::download('public/tugas_s/upload/'.$file);
        }else{
            return back();
        }
    }

    public function destroyTugasSiswa($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id, $siswa_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::find($tugas_id);
            if($tugas->user_s()->where('user_id', $siswa_id )->exists()){
                Storage::delete('public/tugas_s/upload/'.$tugas->user_s()->where('user_id', $siswa_id)->first()->pivot->file_path);
                $tugas->user_s()->detach($siswa_id);
                return redirect()->route('showTugas', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id])->with('done', 'successfully destroyed !!');
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function nilaiTugasSiswa(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id, $siswa_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::find($tugas_id);
            if($tugas->user_s()->where('user_id', $siswa_id )->exists()){
                $tugas->user_s()->updateExistingPivot($siswa_id, [
                    'nilai' => $request->nilai,
                    'status' => 1,
                ]);
                return redirect()->route('showTugas', [$slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id])->with('done', 'successfully createed !!');
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function upload(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tgl = date("Y-m-d H:i:s");

        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $tugas = Tugas::find($tugas_id);
            if($request->hasFile('file')){
                if($tugas->user_s()->where('user_id', Auth::user()->id )->exists()){
                    return back();
                }else{
                    $file = $request->file('file');
                    $filename = $file->getClientOriginalName();
                    $newName = 'User_' . Auth::user()->id . ' ' . 'Upload Tugas_' . $tugas->id . ' ' . $filename;
                    $path = Storage::putFileAs('public/tugas_s/upload', $request->file('file'), $newName);
                    $tugas->user_s()->attach(Auth::user()->id, ['tanggal' => $tgl, 'file_path' => $newName]);
                    $tugas->save();
                    return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
                }
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::find($tugas_id);
            Storage::delete('public/tugas_s/'.$tugas->file_path);
            $tugas->update($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Tugas_' . $tugas->id . ' ' . $filename;
                $path = Storage::putFileAs('public/tugas_s', $request->file('file'), $newName);
                $tugas->file_path = $newName;
            }
            $tugas->save();
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug_hari, $kelas_id, $mapel_id, $materi_id, $tugas_id)
    {
        if(session('role') === 'Guru'){
            $tugas = Tugas::find($tugas_id);
            Storage::delete('public/tugas_s/'.$tugas->file_path);
            $tugas->delete($tugas);
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully destroyed !!');
        }else{
            return back();
        }
    }
}
