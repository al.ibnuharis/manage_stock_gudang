<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Pengumuman;

class PengumumanController extends Controller
{
    public function index()
    {
        $pengumumans = Pengumuman::all();
        return view('backend/dir_pengumuman/index', compact('pengumumans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        return view('backend/dir_pengumuman/action', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pengumuman = Pengumuman::create($request->all());
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $newName = 'Pengumuman_' . $pengumuman->id . ' ' . $filename;
            $path = Storage::putFileAs('public/pengumuman_s', $request->file('file'), $newName);
            $pengumuman->foto = $newName;
        }
        $pengumuman->save();
        return redirect()->route('indexPengumuman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pengumuman_id)
    {
        $pengumuman = Pengumuman::find($pengumuman_id);
        return view('backend/dir_pengumuman/show', compact('pengumuman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($pengumuman_id)
    {
        $pengumuman = Pengumuman::find($pengumuman_id);
        $action = 'edit';
        return view('backend/dir_pengumuman/action', compact('action', 'pengumuman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pengumuman_id)
    {
        $pengumuman = Pengumuman::find($pengumuman_id);
        $foto_old = $pengumuman->foto;
        $pengumuman->update($request->all());
        if($request->hasFile('file')){
            Storage::delete('public/pengumuman_s/'.$foto_old);
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $newName = 'Pengumuman_' . $pengumuman->id . ' ' . $filename;
            $path = Storage::putFileAs('public/pengumuman_s', $request->file('file'), $newName);
            $pengumuman->foto = $newName;
        }
        $pengumuman->save();
        return redirect()->route('indexPengumuman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pengumuman_id)
    {
        $pengumuman = Pengumuman::find($pengumuman_id);
        Storage::delete('public/pengumuman_s/'.$pengumuman->foto);
        $pengumuman->delete($pengumuman);
        return redirect()->route('indexPengumuman');
    }
}
