<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Kelas;
use App\Models\Level;
use App\Models\Hari;
use App\Models\Tahun;
use App\Models\Mapel;
use App\Models\Jadwal;

class JadwalController extends Controller
{
    public function store(Request $request, $kelas_id, $hari_id){
        if(session('role') === 'Operator'){
            if(Jadwal::where('kelas_id', $kelas_id)->where('hari_id', $hari_id)->where('mapel_id', $request->mapel_id)->exists()){
                return redirect()->route('showHariKelas', [$kelas_id, $hari_id]);
            }
            $jadwal = Jadwal::create($request->all());
            $tahun_active = Tahun::where('is_active', 1)->first();
            $jadwal->kelas_id = $kelas_id;
            $jadwal->tahun_id = $tahun_active->id;
            $jadwal->hari_id = $hari_id;
            $jadwal->save();
            return redirect()->route('showHariKelas', [$kelas_id, $hari_id]);
        }else{
            return back();
        }
    }

    public function update(Request $request, $kelas_id, $hari_id, $jadwal_id){
        if(session('role') === 'Operator'){
            $jadwal = Jadwal::find($jadwal_id);
            $jadwal->update($request->all());
            $jadwal->kelas_id = $kelas_id;
            $jadwal->hari_id = $hari_id;
            $jadwal->save();
            return redirect()->route('showHariKelas', [$kelas_id, $hari_id]);
        }else{
            return back();
        }
    }

    public function insertGuru(Request $request, $kelas_id, $hari_id, $jadwal_id){
        if(session('role') === 'Operator'){
            $jadwal = Jadwal::find($jadwal_id);
            $jadwal->user_id = $request->user_id;
            $jadwal->save();
            return redirect()->route('showHariKelas', [$kelas_id, $hari_id]);
        }else{
            return back();
        }
    }
}
