<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Materi;
use App\Models\Hari;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Ebook;
use App\Models\Tugas;

class MateriController extends Controller
{
    public function index($slug_hari, $kelas_id, $mapel_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi_s = Materi::where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->orderBy('tanggal','asc')->get();
            return view('backend/dir_materi/index', compact('hari', 'materi_s', 'mapel', 'kelas'));
        }else{
            return back();
        }
    }

    public function create($slug_hari, $kelas_id, $mapel_id)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $action = 'create';
            return view('backend/dir_materi/action', compact('hari', 'action', 'mapel', 'kelas'));
        }else{
            return back();
        }
    }

    public function store(Request $request, $slug_hari, $kelas_id, $mapel_id)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);

            $materi = Materi::create($request->all());
            if($request->banner === '0'){
                $materi->foto = NULL;
                $materi->video = $request->video;
            }elseif($request->banner === '1'){
                if($request->hasFile('file')){
                    $file = $request->file('file');
                    $filename = $file->getClientOriginalName();
                    $newName = 'Materi_' . $materi->id . ' ' . $filename;
                    $path = Storage::putFileAs('public/materi_s', $request->file('file'), $newName);
                    $materi->foto = $newName;
                    $materi->video = NULL;
                }else{
                    $materi->video = NULL; 
                    $materi->foto = NULL;
                }
            }else{
                $materi->video = NULL; 
                $materi->foto = NULL;
            }
            $materi->save();
            return redirect()->route('indexMateri', [$slug_hari, $kelas_id, $mapel_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    public function show($slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $ebooks = Ebook::where('materi_id', $materi->id)->get();
            return view('backend/dir_materi/show', compact('hari', 'materi', 'mapel', 'kelas', 'ebooks'));
        }else{
            return back();
        }
    }

    public function edit($slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $action = 'edit';
            return view('backend/dir_materi/action', compact('action', 'hari', 'materi', 'mapel', 'kelas'));
        }else{
            return back();
        }
    }

    public function update(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);

            $materi = Materi::find($materi_id);
            $materi->update($request->all());
            $file_old = $materi->foto;

            if($request->banner === '0'){ //link
                $materi->video = $request->video;
            }elseif($request->banner === '1'){ //file
                if($request->hasFile('file')){
                    if($file_old !== NULL){
                        Storage::delete('public/materi_s/'.$file_old);
                    }
                    $file = $request->file('file');
                    $filename = $file->getClientOriginalName();
                    $newName = 'Materi_' . $materi->id . ' ' . $filename;
                    $path = Storage::putFileAs('public/materi_s', $request->file('file'), $newName);
                    $materi->foto = $newName;
                }
            }else{
                $materi->video = NULL; 
                $materi->foto = NULL;
            }
            $materi->save();
            return redirect()->route('indexMateri', [$slug_hari, $kelas_id, $mapel_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    public function destroy($slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $hari = Hari::where('slug', $slug_hari)->first();
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $materi = Materi::find($materi_id);
            $oldFile = $materi->foto;
            Storage::delete('public/materi_s/'.$oldFile);
            $materi->delete($materi);
            return redirect()->route('indexMateri', [$slug_hari, $kelas_id, $mapel_id])->with('done', 'successfully destroyed !!');
        }else{
            return back();
        }
    }
}
