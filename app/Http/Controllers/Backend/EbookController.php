<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\Materi;
use App\Models\Hari;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Ebook;

class EbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id)
    {
        if(session('role') === 'Guru'){
            $ebook = Ebook::create($request->all());
            if($request->hasFile('file')){
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Ebook_' . $ebook->id . ' ' . $filename;
                $path = Storage::putFileAs('public/ebook_s', $request->file('file'), $newName);
                $ebook->file_path = $newName;
            }
            $ebook->save();
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $slug_hari, $kelas_id, $mapel_id, $materi_id, $ebook_id)
    {
        if(session('role') === 'Guru'){
            $ebook = Ebook::find($ebook_id);
            $file_old = $ebook->file_path;
            $ebook->update($request->all());
            if($request->hasFile('file')){
                if($file_old !== NULL){
                    Storage::delete('public/ebook_s/'.$file_old);
                }
                $file = $request->file('file');
                $filename = $file->getClientOriginalName();
                $newName = 'Ebook_' . $ebook->id . ' ' . $filename;
                $path = Storage::putFileAs('public/ebook_s', $request->file('file'), $newName);
                $ebook->file_path = $newName;
            }
            $ebook->save();
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully created !!');
        }else{
            return back();
        }
    }

    public function destroy($slug_hari, $kelas_id, $mapel_id, $materi_id, $ebook_id)
    {
        if(session('role') === 'Guru'){
            $ebook = Ebook::find($ebook_id);
            Storage::delete('public/ebook_s/'.$ebook->file_path);
            $ebook->delete($ebook);
            return redirect()->route('showMateri', [$slug_hari, $kelas_id, $mapel_id, $materi_id])->with('done', 'successfully destroyed !!');
        }else{
            return back();
        }
    }

    public function download($slug_hari, $kelas_id, $mapel_id, $materi_id, $ebook_id)
    {
        if(session('role') === 'Guru' || session('role') === 'Siswa'){
            $ebook = Ebook::find($ebook_id);
            return Storage::download('public/ebook_s/'.$ebook->file_path);
        }else{
            return back();
        }
    }
}
