<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tugas extends Model
{
    protected $dates = ['start','expired'];
    protected $fillable = ['user_id', 'materi_id', 'nama_tugas', 'file_path', 'keterangan', 'start', 'expired'];

    public function materi()
    {
        return $this->belongsTo(Materi::class, 'materi_id');
    }

    public function user_s(){
        return $this->belongsToMany('App\Models\User', 'hasil_tugas')->withTimeStamps()->withPivot(['id', 'user_id', 'tugas_id', 'tanggal', 'file_path', 'status', 'nilai']);
    }
}
