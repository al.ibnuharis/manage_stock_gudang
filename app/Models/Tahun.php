<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $fillable = ['tahun'];

    // relasi many to one tahun ke kelas (satu tahun bisa di miliki banyak kelas)
    public function kelas_s(){
        return $this->hasMany(Kelas::class);    
    }
    // relasi many to one tahun ke mapel (satu tahun bisa di miliki banyak mapel)
    public function mapel_s(){
        return $this->hasMany(Mapel::class);    
    }
}
