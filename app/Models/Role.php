<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    // relasi many to one role ke user (satu role bisa di miliki banyak users)
    public function users(){
        return $this->hasMany(User::class);    
    }
}
