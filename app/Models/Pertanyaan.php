<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $fillable = ['user_id', 'tipe_pertanyaan_id', 'mapel_id', 'pertanyaan', 'pilihan', 'jawaban'];
    // relasi user ke role (one to many atau 1 kelas hanya punya 1 tahun pelajaran)
    public function tipe()
    {
        return $this->belongsTo('App\Models\TipePertanyaan', 'tipe_pertanyaan_id');
    }
}
