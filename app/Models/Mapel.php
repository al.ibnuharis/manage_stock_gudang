<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $fillable = ['level_id', 'tahun_id', 'nama_mapel', 'slug', 'file', 'batas_kkm'];

    public function kelas_s(){
        return $this->belongsToMany('App\Models\Kelas', 'kelas_mapel')->withTimeStamps()->withPivot(['id'],['mapel_id'], ['kelas_id']);
    }

    // relasi user ke role (one to many atau 1 kelas hanya punya 1 tahun pelajaran)
    public function tahun()
    {
        return $this->belongsTo(Tahun::class);
    }

    // relasi many to many guru dengan mapel
    public function guru_s(){
        return $this->belongsToMany('App\Models\User', 'guru_mapel')->withTimeStamps()->withPivot(['id'], ['user_id'], ['mapel_id']);
    }

    // relasi user ke role (one to many atau 1 kelas hanya punya 1 tahun pelajaran)
    public function segments()
    {
        return $this->hasMany(Segment::class);
    }
}
