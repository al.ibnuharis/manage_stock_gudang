<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','role_id', 'email', 'phone', 'nip_guru', 'nis_siswa', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'kota', 'nomor', 'kode_pos', 'pendidikan_guru', 'status', 'jabatan', 'nik', 'agama', 'password', 'foto', 'slug', 'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    // relasi user ke role (one to many atau 1 user punya 1 role)
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    // relasi many to many siswa dengan kelas
    public function kelas_s(){
        return $this->belongsToMany('App\Models\Kelas', 'kelas_siswa')->withTimeStamps()->withPivot(['id'],['user_id'], ['kelas_id'], ['is_active']);
    }

    // relasi many to many guru dengan mapel
    public function mapel_s(){
        return $this->belongsToMany('App\Models\Mapel', 'guru_mapel')->withTimeStamps()->withPivot(['id'],['user_id'], ['mapel_id']);
    }

    // relasi many to one role ke user (satu role bisa di miliki banyak users)
    public function materi_s(){
        return $this->hasMany(Materi::class, 'user_id');    
    }

    // relasi many to many guru dengan mapel
    public function tugas_s(){
        return $this->belongsToMany('App\Models\Tugas', 'hasil_tugas')->withTimeStamps()->withPivot(['id', 'user_id', 'tugas_id', 'tanggal', 'file_path', 'status', 'nilai']);
    }
}
