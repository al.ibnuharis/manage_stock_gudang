@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row my-4">
  <div class="col-12 col-xl-12 mb-4">
    <div class="row">
      <div class="col-12 mb-4">
        <div class="card border-light shadow-sm">
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col-auto">
                  <span class="icon icon-lg text-purple scale"><span class="fas fa-book"></span></span>
              </div>
              <div class="col">
                <div class="progress-wrapper">
                  <div class="progress-info">
                    <div class="h6 mb-0">{{ $mapel->nama_mapel }}</div>
                    <div class="small font-weight-bold text-dark"><span>{{ $mapel->batas_kkm}}</span></div>
                  </div>
                  <div class="progress mb-0">
                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="{{ $mapel->batas_kkm }}"
                      aria-valuemin="0" aria-valuemax="100" style="width: {{ $mapel->batas_kkm }}%;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 mb-4">
        <div class="card border-light shadow-sm mb-4">
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col text-left">
                <span class="icon icon-lg text-primary"><span class="fas fa-chalkboard-teacher"></span></span>
              </div>
              <div class="col-auto">
                <div class="btn-toolbar dropdown">
                  <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-plus mr-2"></span>New Task</button>
                  <div class="dropdown-menu dashboard-dropdown dropdown-menu-right mt-2">
                    <a class="dropdown-item font-weight-bold" href=""><span class="fas fa-plus"></span>Create Guru</a> 
                    <button type="button" data-toggle="modal" data-target="#modal-guru" class="dropdown-item font-weight-bold" href=""><span class="fas fa-caret-left"></span>Insert Guru</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card border-light shadow-sm">
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Nama</th>
                  <th scope="col">NIP Guru</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($mapel->guru_s as $mapelGuru)
                  <tr>
                    <th scope="row">{{ $mapelGuru->name }}</th>
                    <td>{{ $mapelGuru->nip_guru }}</td>
                    <td>{{ $mapelGuru->email }}</td>
                    <td>{{ $mapelGuru->phone }}</td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item text-danger" href="{{ route('outGuruMapel', [$mapel->id, $mapelGuru->id]) }}"><span class="fas fa-user-times mr-2"></span>Take Out</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
      <div class="col-md-4">
        <div class="modal fade" id="modal-guru" tabindex="-1" role="dialog" aria-labelledby="modal-form-signup" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-body p-0">
                <div class="card border-light p-4">
                  <div class="card-header text-center pb-0">
                    <h2 class="mb-0 h5">Insert Guru</h2>
                  </div>
                  <div class="card-body">
                    <form action="{{ route('insertGuruMapel', $mapel->id) }}" method="POST">
                      @csrf
                      <div class="form-group mb-4">
                        <select id="state_2" class="w-100" name="guru_id">
                          @foreach ($guru_s as $guru)
                          <option value="{{ $guru->id }}">{{ $guru->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <button type="submit" class="btn btn-block btn-primary">Insert</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

</div>

{{-- konten --}}
@endsection