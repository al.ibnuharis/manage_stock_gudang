@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
  <div class="d-block mb-4 mb-md-0">
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
      <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><span class="fas fa-home"></span></a></li>
        <li class="breadcrumb-item active" aria-current="page">Mata Pelajaran</li>
      </ol>
    </nav>
    <h2 class="h4">All Mapel</h2>
    <p class="mb-0">Semua dafter mataplajaran kelas 1 sampai 6.</p>
  </div>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group">
      <button type="button" class="btn btn-sm btn-outline-primary">Print</button>
      <button type="button" class="btn btn-sm btn-outline-primary">Export</button>
    </div>
  </div>
</div>
<div class="table-settings mb-5">
  <div class="row align-items-center justify-content-between">
    <div class="col col-md-6 col-lg-3 col-xl-4">
      <div class="btn-toolbar dropdown">
        <button class="btn btn-primary btn-sm mr-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-plus mr-2"></span>New Task</button>
        <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
          <a class="dropdown-item font-weight-bold" href="{{ route('createMapel') }}"><span class="fas fa-tasks"></span>Tambah Mapel</a> 
          <a class="dropdown-item font-weight-bold" href=""><span class="fas fa-cloud-upload-alt"></span>Inport Mapel</a>
        </div>
      </div>
    </div>
    <div class="col-4 col-md-2 col-xl-1 pl-md-0 text-right">
      <div class="btn-group">
        <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="icon icon-sm icon-gray"><span class="fas fa-cog"></span></span><span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
          <span class="dropdown-item font-weight-bold text-dark">Show</span>
          <a class="dropdown-item d-flex font-weight-bold" href="#">10 <span class="icon icon-small ml-auto"><span class="fas fa-check"></span></span></a>
          <a class="dropdown-item font-weight-bold" href="#">20</a> <a class="dropdown-item font-weight-bold" href="#">30</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row mb-3">
  @foreach ($mapel_s as $mapel)
  <div class="col-12 col-lg-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-auto">
            <a href="{{ route('showMapel', $mapel->id) }}" class="page-preview scale-up-hover-2">
              <span class="icon icon-md text-purple scale"><span class="fas fa-book"></span></span>
            </a>
          </div>
          <div class="col">
            <div class="progress-wrapper">
              <div class="progress-info">
                <div class="h6 mb-0">{{ $mapel->nama_mapel }}</div>
                <div class="small font-weight-bold text-dark"><span>{{ $mapel->batas_kkm}}</span></div>
              </div>
              <div class="progress mb-0">
                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="{{ $mapel->batas_kkm }}"
                  aria-valuemin="0" aria-valuemax="100" style="width: {{ $mapel->batas_kkm }}%;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
{{-- konten --}}
@endsection