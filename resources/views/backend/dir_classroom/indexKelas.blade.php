@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
  <div class="d-block mb-4 mb-md-0">
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
      <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><span class="fas fa-home"></span></a></li>
        <li class="breadcrumb-item active" aria-current="page">Daftar Kelas</li>
      </ol>
    </nav>
    <p class="mb-0">Jadwal Kelas Anda pada hari <b>{{ $hari->nama_hari}}</b>.</p>
  </div>
</div>


<div class="row mb-4">
  @foreach ($jadwal_s as $jadwal)
    @if(session('role') === 'Guru')
      <div class="col-12 col-sm-6 col-xl-4 mb-4">
        <div class="card border-light shadow-sm">
          <div class="card-body">
            <div class="row d-block d-xl-flex align-items-center">
              <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                <a href="{{ route('indexMateri', [$hari->slug, $jadwal->kelas->id, $jadwal->mapel->id])}}" class="page-preview scale-up-hover-2">
                  <div class="icon icon-shape icon-md icon-shape-blue rounded mr-4 mr-sm-0 scale">
                    <span class="fas fa-inbox"></span>
                  </div>
                </a>
                <div class="d-sm-none">
                  <h2 class="h5"></h2>
                  <h3 class="mb-1">69</h3>
                </div>
              </div>
              <div class="col-12 col-xl-8 px-xl-0">
                <div class="d-none d-sm-block">
                  <h2 class="h5"></h2>
                  <h3 class="mb-1">{{ $jadwal->kelas->nama_kelas}}</h3>
                </div>
                <small>{{ $jadwal->mapel->nama_mapel}}</small>
                <div class="mt-1">
                  <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0 small"><span class="fas fa-clock mr-2"></span>{{ date('h:i A', strtotime($jadwal->jam_mulai)) }} - {{ date('h:i A', strtotime($jadwal->jam_akhir)) }}</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @elseif(session('role') === 'Siswa')
      <div class="col-12 col-sm-6 col-xl-4 mb-4">
        <div class="card border-light shadow-sm">
          <div class="card-body">
            <div class="row d-block d-xl-flex align-items-center">
              <div class="col-12 col-xl-4 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                <a href="{{ route('indexMateri', [$hari->slug, $jadwal->kelas->id, $jadwal->mapel->id])}}" class="page-preview scale-up-hover-2">
                  <div class="icon icon-shape icon-md icon-shape-blue rounded mr-4 mr-sm-0 scale">
                    <span class="fas fa-book"></span>
                  </div>
                </a>
                <div class="d-sm-none">
                  <h2 class="h5"></h2>
                  <h3 class="mb-1">69</h3>
                </div>
              </div>
              <div class="col-12 col-xl-8 px-xl-0">
                <div class="d-none d-sm-block">
                  <h2 class="h5"></h2>
                  <h5 class="mb-1">{{ $jadwal->mapel->nama_mapel}}</h5>
                </div>
                <small>Guru : {{ $jadwal->user->name}}</small>
                <div class="mt-1">
                  <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0 small"><span class="fas fa-clock mr-2"></span>{{ date('h:i A', strtotime($jadwal->jam_mulai)) }} - {{ date('h:i A', strtotime($jadwal->jam_akhir)) }}</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif
  @endforeach

</div>
{{-- konten --}}
@endsection