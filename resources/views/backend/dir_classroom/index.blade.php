@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row mt-5">

  @foreach ($haris as $hari)
  <div class="col-sm-6 col-lg-4">
    <a href="{{ route('showClassroom', $hari->slug)}}">
      <div class="card bg-white shadow-soft text-primary rounded mb-4">
        <div class="px-3 px-lg-4 py-5 text-center">
          <span class="icon icon-lg mb-4"><span class="far fa-calendar-alt"></span></span>
          <p>Jadwal Hari</p>
          <h5 class="font-weight-bold text-primary">{{ $hari->nama_hari }}</h5>
        </div>
      </div>
    </a>
  </div>
  @endforeach

</div>
{{-- konten --}}
@endsection