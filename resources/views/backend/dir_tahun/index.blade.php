@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
  <div class="d-block mb-4 mb-md-0">
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
      <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><span class="fas fa-home"></span></a></li>
        <li class="breadcrumb-item active" aria-current="page">Tahun Pelajaran</li>
      </ol>
    </nav>
    <h2 class="h4">Tahun Ajar</h2>
    <p class="mb-0">Tahun pelajaran adalah masa belajar dalam tahun tertentu</p>
  </div>
</div>
<div class="table-settings mb-5">
  <div class="row align-items-center justify-content-between">
    <div class="col col-md-6 col-lg-3 col-xl-4">
      <div class="btn-toolbar dropdown">
        <button type="button" data-toggle="modal" data-target="#modal-create" class="btn btn-primary btn-sm mr-2"><span class="fas fa-plus mr-2"></span>Tambah Tahun</button>
      </div>
    </div>
    <div class="col-4 col-md-2 col-xl-1 pl-md-0 text-right">
      <div class="btn-group">
        <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="icon icon-sm icon-gray"><span class="fas fa-cog"></span></span><span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
          <span class="dropdown-item font-weight-bold text-dark">Show</span>
          <a class="dropdown-item d-flex font-weight-bold" href="#">10 <span class="icon icon-small ml-auto"><span class="fas fa-check"></span></span></a>
          <a class="dropdown-item font-weight-bold" href="#">20</a> <a class="dropdown-item font-weight-bold" href="#">30</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-4"> 
  <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-create" aria-hidden="true">
    <div class="modal-dialog modal-tertiary modal-dialog-centered modal-lg" role="document">
      <div class="modal-content bg-dark text-white">
        <div class="modal-header">
          <button type="button" class="close ml-auto" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body text-center py-3"><span class="modal-icon display-1-lg"><span class="fas fa-vote-yea"></span></span>
          <h3 class="modal-title mb-3">Buat Tahun Pelajaran</h3>
          <div class="form-group px-5">
            <form action="{{ route('storeTahunAjar') }}" method="POST">
              @csrf
              <div class="d-sm-flex flex-column flex-sm-row mb-3 justify-content-center">
                <input name="tahun" type="number" class="mr-sm-1 mb-2 mb-sm-0 form-control form-control-lg" placeholder="Tahun pelajaran">
                <button type="submit" class="ml-sm-1 btn btn-lg btn-tertiary">Sumbit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row mb-3">
  @foreach ($tahun_s as $tahun)
  <div class="col-md-4 mb-4">
    {{-- modal --}}
    <div class="col-md-4">
      <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
        <div class="modal-dialog modal-tertiary modal-dialog-centered modal-lg" role="document">
          <div class="modal-content bg-dark text-white">
            <div class="modal-header">
              <button type="button" class="close ml-auto" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body text-center py-3"><span class="modal-icon display-1-lg"><span class="fas fa-vote-yea"></span></span>
              <h3 class="modal-title mb-3">Buat Tahun Pelajaran</h3>
              <div class="form-group px-5">
                <form action="{{ route('updateTahunAjar', $tahun->id) }}" method="POST">
                  @csrf
                  <div class="d-sm-flex flex-column flex-sm-row mb-3 justify-content-center">
                    <input name="tahun" type="number" class="mr-sm-1 mb-2 mb-sm-0 form-control form-control-lg" placeholder="Tahun pelajaran" value="{{ $tahun->tahun }}">
                    <button type="submit" class="ml-sm-1 btn btn-lg btn-tertiary">Sumbit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- list --}}
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row d-block d-md-flex align-items-center">
          <div class="col-md-9">
            <h2 class="h5 mb-2">Tahun Pelajaran</h2>
            <h2 class="display-4">{{ $tahun->tahun }}@if($tahun->is_active == 1)<span class="pixel-pro-badge subscription-badge bg-success font-weight-bolder text-white py-0">Active</span>@endif</h2>
            <span class="small">
              <div class="btn-group">
                <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                  <button type="button" data-toggle="modal" data-target="#modal-edit" class="dropdown-item" href=""><span class="fas fa-user-edit mr-2"></span>Edit Data</button>
                  @if($tahun->is_active == 0)
                  <a href="{{ route('activeTahunAjar', $tahun->id) }}" class="dropdown-item text-success"><span class="fas fa-user-check mr-2"></span>Set Active</a>
                  @endif
                </div>
              </div>
            </span>
          </div>
          <div class="col-md-3 mt-3 mt-md-0">
            <div class="col-12">
              <div class="row d-flex align-items-center mb-1">
                <div class="col-12 px-0">
                  <span class="small"><span class="font-weight-bold text-dark">0</span> Kelas</span>
                </div>
              </div>
              <div class="row d-flex align-items-center mb-1">
                <div class="col-12 px-0">
                  <span class="small"><span class="font-weight-bold text-dark">0</span> Guru</span>
                </div>
              </div>
              <div class="row d-flex align-items-center mb-1">
                <div class="col-12 px-0">
                  <span class="small"><span class="font-weight-bold text-dark">0</span> Siswa</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
{{-- konten --}}
@endsection