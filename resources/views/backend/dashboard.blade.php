@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}

<div class="row justify-content-md-center mt-4">
  <div class="col-12 col-sm-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row d-block d-xl-flex align-items-center">
          <div
            class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
            <div class="icon icon-shape icon-md icon-shape-blue rounded mr-4 mr-sm-0"><span
                class="fas fa-chart-line"></span></div>
            <div class="d-sm-none">
              <h2 class="h5">Customers</h2>
              <h3 class="mb-1">345,678</h3>
            </div>
          </div>
          <div class="col-12 col-xl-7 px-xl-0">
            <div class="d-none d-sm-block">
              <h2 class="h5">Customers</h2>
              <h3 class="mb-1">345k</h3>
            </div><small>Feb 1 - Apr 1, <span class="icon icon-small"><span
                  class="fas fa-globe-europe"></span></span> WorldWide</small>
            <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
                class="text-success font-weight-bold">18.2%</span> Since last month</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row d-block d-xl-flex align-items-center">
          <div
            class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
            <div class="icon icon-shape icon-md icon-shape-secondary rounded mr-4"><span
                class="fas fa-cash-register"></span></div>
            <div class="d-sm-none">
              <h2 class="h5">Revenue</h2>
              <h3 class="mb-1">$43,594</h3>
            </div>
          </div>
          <div class="col-12 col-xl-7 px-xl-0">
            <div class="d-none d-sm-block">
              <h2 class="h5">Revenue</h2>
              <h3 class="mb-1">$43,594</h3>
            </div><small>Feb 1 - Apr 1, <span class="icon icon-small"><span
                  class="fas fa-globe-europe"></span></span> Worldwide</small>
            <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
                class="text-success font-weight-bold">28.2%</span> Since last month</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body">
        <div class="row d-block d-xl-flex align-items-center">
          <div
            class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
            <div class="ct-chart-traffic-share ct-golden-section ct-series-a"></div>
          </div>
          <div class="col-12 col-xl-7 px-xl-0">
            <h2 class="h5 mb-3">Traffic Share</h2>
            <h6 class="font-weight-normal text-gray"><span class="icon w-20 icon-xs icon-secondary mr-1"><span
                  class="fas fa-desktop"></span></span> Desktop <a href="#" class="h6">60%</a></h6>
            <h6 class="font-weight-normal text-gray"><span class="icon w-20 icon-xs icon-primary mr-1"><span
                  class="fas fa-mobile-alt"></span></span> Mobile Web <a href="#" class="h6">30%</a></h6>
            <h6 class="font-weight-normal text-gray"><span class="icon w-20 icon-xs icon-tertiary mr-1"><span
                  class="fas fa-tablet-alt"></span></span> Tablet Web <a href="#" class="h6">10%</a></h6>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
        <div class="d-block">
          <div class="h6 font-weight-normal text-gray mb-2">Total orders</div>
          <h2 class="h3">452</h2>
          <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
              class="text-success font-weight-bold">18.2%</span></div>
        </div>
        <div class="d-block ml-auto">
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-quaternary mr-2"></span> <span
              class="font-weight-normal small">July</span></div>
          <div class="d-flex align-items-center text-right"><span
              class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
              class="font-weight-normal small">August</span></div>
        </div>
      </div>
      <div class="card-body p-2">
        <div class="ct-chart-ranking ct-golden-section ct-series-a"></div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
        <div class="d-block">
          <div class="h6 font-weight-normal text-gray mb-2">Traffic by Source</div>
          <h2 class="h4">Google</h2>
          <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
              class="text-success font-weight-bold">10.57%</span></div>
        </div>
        <div class="d-block ml-auto">
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-quaternary mr-2"></span> <span
              class="font-weight-normal small">Google</span></div>
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
              class="font-weight-normal small">Yahoo</span></div>
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-tertiary mr-2"></span> <span
              class="font-weight-normal small">Yandex</span></div>
        </div>
      </div>
      <div class="card-body p-2">
        <div class="ct-chart-traffic-source ct-golden-section ct-series-a"></div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-6 col-xl-4 mb-4">
    <div class="card border-light shadow-sm">
      <div class="card-body d-flex flex-row align-items-center flex-0 border-bottom">
        <div class="d-block">
          <div class="h6 font-weight-normal text-gray mb-2">Organic vs Paid Search</div>
          <h2 class="h4">Trafic Distibution</h2>
          <div class="small mt-2"><span class="fas fa-angle-up text-success"></span> <span
              class="text-success font-weight-bold">$10.57%</span></div>
        </div>
        <div class="d-block ml-auto">
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-secondary mr-2"></span> <span
              class="font-weight-normal small">Organic</span></div>
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-tertiary mr-2"></span> <span
              class="font-weight-normal small">Direct</span></div>
          <div class="d-flex align-items-center text-right mb-2"><span
              class="shape-xs rounded-circle bg-primary mr-2"></span> <span
              class="font-weight-normal small">Paid</span></div>
        </div>
      </div>
      <div class="card-body p-2">
        <div class="ct-chart-distribution ct-golden-section ct-series-a"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 col-xl-8 mb-4">
    <div class="row">
      <div class="col-12 mb-4">
        <div class="card border-light shadow-sm">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col">
                <h2 class="h5">Page visits</h2>
              </div>
              <div class="col text-right"><a href="./traffic-sources.html"
                  class="btn btn-sm btn-secondary">See all</a></div>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Page name</th>
                  <th scope="col">Page Views</th>
                  <th scope="col">Page Value</th>
                  <th scope="col">Bounce rate</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">/demo/admin/index.html</th>
                  <td>3,225</td>
                  <td>$20</td>
                  <td><span class="fas fa-arrow-up text-danger mr-3"></span> 42,55%</td>
                </tr>
                <tr>
                  <th scope="row">/demo/admin/forms.html</th>
                  <td>2,987</td>
                  <td>0</td>
                  <td><span class="fas fa-arrow-down text-success mr-3"></span> 43,52%</td>
                </tr>
                <tr>
                  <th scope="row">/demo/admin/util.html</th>
                  <td>2,844</td>
                  <td>294</td>
                  <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,35%</td>
                </tr>
                <tr>
                  <th scope="row">/demo/admin/validation.html</th>
                  <td>2,050</td>
                  <td>$147</td>
                  <td><span class="fas fa-arrow-up text-danger mr-3"></span> 50,87%</td>
                </tr>
                <tr>
                  <th scope="row">/demo/admin/modals.html</th>
                  <td>1,483</td>
                  <td>$19</td>
                  <td><span class="fas fa-arrow-down text-success mr-3"></span> 32,24%</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-xl-4 mb-4">
    <div class="col-12 px-0 mb-4">
      <div class="card border-light shadow-sm">
        <div class="card-body">
          <h2 class="h5">Acquisition</h2>
          <p>Tells you where your visitors originated from, such as search engines, social networks or website
            referrals.</p>
          <div class="d-block">
            <div class="d-flex align-items-center pt-3 mr-5">
              <div class="icon icon-shape icon-sm icon-shape-danger rounded mr-3"><span
                  class="fas fa-chart-bar"></span></div>
              <div class="d-block"><label class="mb-0">Bounce Rate</label>
                <h4 class="mb-0">33.50%</h4>
              </div>
            </div>
            <div class="d-flex align-items-center pt-3">
              <div class="icon icon-shape icon-sm icon-shape-quaternary rounded mr-3"><span
                  class="fas fa-chart-area"></span></div>
              <div class="d-block"><label class="mb-0">Sessions</label>
                <h4 class="mb-0">9,567</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection