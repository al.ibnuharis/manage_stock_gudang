@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
  <div class="d-block mb-3 mb-md-0">
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
      <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><span class="fas fa-home"></span></a></li>
        <li class="breadcrumb-item active" aria-current="page">Daftar Kelas</li>
      </ol>
    </nav>
    <p class="mb-0">Hari {{ $hari->nama_hari}} <b>Pelajaran {{ $mapel->nama_mapel }}</b>.</p>
  </div>
</div>
@if(session('role') === 'Guru')
<div class="row mb-1">
  <div class="d-flex justify-content-between align-items-center pb-2 pt-4 pb-md-4">
    <div>
      <a href="{{ route('createMateri', [$hari->slug, $kelas->id, $mapel->id]) }}" class="btn btn-secondary text-dark mb-3 mb-md-0"><span class="fas fa-plus mr-2"></span><span>Buat Materi</span></a>
    </div>
  </div>
</div>
@endif
<div class="message-wrapper border bg-white border-light shadow-sm p-1 rounded">
  @foreach ($materi_s as $materi)
  <div class="card hover-state border-bottom rounded-0 py-3">
    <div class="card-body d-flex align-items-center flex-wrap flex-lg-nowrap py-0">
      <div class="col-10 col-lg-3 pl-0">
        <a href="{{ route('showMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" class="d-flex align-items-center">
          <div class="user-avatar xs-avatar bg-secondary mr-3"><span>{{ $loop->iteration }}</span></div>
          <span class="h6 font-weight-normal mb-0">{{ $materi->guru->name }}</span>
        </a>
      </div>
      <div class="col-2 col-lg-3 d-flex align-items-center justify-content-end px-0 order-lg-4">
        <a href="{{ route('showMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}">
          <div class="text-muted small d-none d-lg-block">{{ date('d F Y', strtotime($materi->tanggal)) }}</div>
        </a>
        @if(session('role') === 'Guru')
          <div class="btn-group ml-3">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ route('editMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}"><span class="fas fa-user-shield mr-2"></span>Edit</a>
              <a class="dropdown-item text-danger" href="{{ route('destroyMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}"><span class="fas fa-user-times mr-2"></span>Delete</a>
            </div>
          </div>
        @endif
      </div>
      <div class="col-12 col-lg-6 d-flex align-items-center mt-3 mt-lg-0 pl-0">
        <a href="{{ route('showMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" class="font-weight-normal text-gray truncate-text">
          <span class="font-weight-normal pl-lg-3">{{ $materi->nama_materi }}</span>
        </a>
      </div>
    </div>
  </div>
  @endforeach

  <div class="row p-4">
    <div class="col-7 mt-1">Showing 1 - 20 of 289</div>
    <div class="col-5">
      <div class="btn-group float-right"><a href="#" class="btn btn-light"><span
            class="fas fa-chevron-left"></span></a> <a href="#" class="btn btn-primary"><span
            class="fas fa-chevron-right"></span></a></div>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection