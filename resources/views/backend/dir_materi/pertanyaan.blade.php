@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  
  <div class="col-12 col-xl-9">
    <form @if($for_as == 'create_kunci') action="{{ route('storeKunciSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}" @else action="{{ route('storeKerjakanSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}" @endif method="POST" enctype="multipart/form-data">
      @csrf
      @php $nomor=1; @endphp
      @foreach ($soal->segments as $segment)
        @foreach ($segment->pertanyaans as $pertanyaan)
          @if ($pertanyaan->tipe_pertanyaan_id === 1)
            <div class="card bg-white border-light p-4 mb-4">
              <div class="d-flex justify-content-between align-items-center mb-2">
                <span class="font-small">
                  <span class="fas fa-microphone mr-2 fa-lg"></span><span class="font-weight-bold">Question {{$nomor}}</span>
                  <span class="ml-2"><i>{{$segment->nama_segment}}</i></span>
                </span>
                <div class="d-none d-sm-block">
                  <button class="btn btn-link text-dark" aria-label="phone" data-toggle="tooltip" data-placement="top" title="{{$segment->nama_segment}}" data-original-title="{{$segment->nama_segment}}">
                    <span class="fas fa-question"></span>
                  </button>
                </div>
              </div>
              <p class="m-0">{{$pertanyaan->pertanyaan}}</p>
              <div class="review_block mt-3">
                <ul>
                  <li>
                    @php $options = preg_split('/\r\n|\r|\n/', $pertanyaan->pilihan); @endphp
                    @foreach ($options as $option)
                      <div class="checkbox_radio_container">
                          <input type="radio" id="{{$pertanyaan->id}}{{$option}}" name="{{$pertanyaan->id}}" class="required" value="{{$option}}" @if($for_as == 'create_kunci') @if($pertanyaan->jawaban == $option) checked @endif @endif>
                          <label class="radio" for="{{$pertanyaan->id}}{{$option}}"></label>
                          <label for="{{$pertanyaan->id}}{{$option}}" class="wrapper">{{$option}}</label>
                      </div>
                    @endforeach
                  </li>
                </ul>
              </div>
            </div>
          @elseif($pertanyaan->tipe_pertanyaan_id === 2)
            <div class="card bg-white border-light p-4 mb-4">
              <div class="d-flex justify-content-between align-items-center mb-2">
                <span class="font-small">
                  <span class="fas fa-microphone mr-2 fa-lg"></span><span class="font-weight-bold">Question {{$nomor}}</span>
                  <span class="ml-2"><i>{{$segment->nama_segment}}</i></span>
                </span>
                <div class="d-none d-sm-block">
                  <button class="btn btn-link text-dark" aria-label="phone" data-toggle="tooltip" data-placement="top" title="{{$segment->nama_segment}}" data-original-title="{{$segment->nama_segment}}">
                    <span class="fas fa-question"></span>
                  </button>
                </div>
              </div>
              <p class="m-0">{{$pertanyaan->pertanyaan}}</p>
              <div class="form-group mt-3">
                  <label for="question_4_label">Jawaban Essay</label>
                  <textarea name="{{$pertanyaan->id}}" id="question_4_label" class="form-control" style="height:180px;" onkeyup="getVals(this, 'question_4');">@if($for_as == 'create_kunci'){{$pertanyaan->jawaban}}@endif</textarea>
              </div>
            </div>
          @php $nomor++ @endphp
          @endif
        @endforeach
      @endforeach
      
      <div class="card bg-soft-indigo border-light">
        <div class="row g-0 align-items-center">
          <div class="col-12 col-lg-12 col-xl-12">
            <div class="card-body text-dark text-center">
                <div class="btn-group text-center">
                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    
  </div>
  
  @include('backend/dir_materi/side')
</div>
@endsection