@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  
  <div class="col-12 col-xl-9">
    <div class="card border-light shadow-sm mb-4">
      <div class="card-body">
        <div class="row d-block d-xl-flex align-items-center">
          <div class="col-12 col-xl-2 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
            <a href="{{ route('createKunciSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}">  
              <div class="icon icon-shape icon-md icon-shape-blue rounded mr-4 mr-sm-0 scale-up-2">
                <span class="fas fa-tasks"></span>
              </div>
            </a>
            <div class="d-sm-none">
              <h2 class="h5"><em> {{ $soal->nama_soal}}</em></h2>
              <h3 class="mb-1">Kunci Jawaban</h3>
            </div>
          </div>
          <div class="col-12 col-xl-10 px-xl-0">
            <div class="d-none d-sm-block">
              <h2 class="h5"><em> {{ $soal->nama_soal}}</em></h2>
              <h3 class="mb-1">Kunci Jawaban</h3>
            </div>
            @php $nomor=0; @endphp @foreach ($soal->segments as $segment) @foreach ($segment->pertanyaans as $pertanyaan) @php $nomor++ @endphp @endforeach @endforeach
            <small>{{$nomor}} Soal</small>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-buat-segment" tabindex="-1" role="dialog" aria-labelledby="modal-buat-segment" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card border-light p-4">
              <div class="card-body">
                <form action="{{ route('storeSegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id])}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="mapel_id" value="{{$mapel->id}}">
                    <div class="col-md-12 mb-3">
                      <div>
                        <label for="nama_segment">Nama Segment</label>
                        <input name="nama_segment" class="form-control" id="nama_segment" type="text" placeholder="Nama Segment" value="" required>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-insert-segment" tabindex="-1" role="dialog" aria-labelledby="modal-insert-segment" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            @foreach ($mapel->segments as $segment)
            <div class="row align-items-center my-4">
              <div class="col text-left">
                <div class="d-block">
                  <span class="font-weight-bold">{{$segment->nama_segment}}</span>
                  <div class="small text-gray">{{ $segment->pertanyaans->count()}} Pertanyaan</div>
                </div>
              </div>
              <div class="col-auto">
                <form action="{{ route('pasangSegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id]) }}" method="POST">
                  @csrf
                  <input type="hidden" name="segment_id" value="{{$segment->id}}">
                  <button class="btn btn-secondary btn-sm" type="submit"><span class="fas fa-plus mr-2"></span>New Task</button>
                </form>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div class="table-settings mb-3">
      <div class="row align-items-center justify-content-between">
        <div class="col col-md-6 col-lg-3 col-xl-4">
          <div class="btn-toolbar dropdown">
            <button class="btn btn-primary btn-sm mr-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-plus mr-2"></span>Segment Pertanyaan</button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-left mt-2">
              <a data-toggle="modal" data-target="#modal-buat-segment" class="dropdown-item font-weight-bold"><span class="fas fa-tasks"></span>Buat Segment</a> 
              <a data-toggle="modal" data-target="#modal-insert-segment" class="dropdown-item font-weight-bold" href=""><span class="fas fa-cloud-upload-alt"></span>Insert Segment</a>
            </div>
          </div>
        </div>
        <div class="col-4 col-md-2 col-xl-1 pl-md-0 text-right">
          <div class="btn-group">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm icon-gray"><span class="fas fa-cog"></span></span><span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
              <span class="dropdown-item font-weight-bold text-dark">Show</span>
              <a class="dropdown-item d-flex font-weight-bold" href="#">10 <span class="icon icon-small ml-auto"><span class="fas fa-check"></span></span></a>
              <a class="dropdown-item font-weight-bold" href="#">20</a> <a class="dropdown-item font-weight-bold" href="#">30</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0 mb-4">
      <table class="table user-table table-hover align-items-center">
        <thead>
          <tr>
            <th class="border-bottom py-4">Segment Soal</th>
            <th class="border-bottom py-4"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($soal->segments as $segment)
            <div class="modal fade" id="modal-edit-segment{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-edit-segment{{$loop->iteration}}" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-body p-0">
                    <div class="card border-light p-4">
                      <div class="card-body">
                        <form action="{{ route('updateSegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id])}}" method="POST" enctype="multipart/form-data">
                          @csrf
                          <div class="row">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="mapel_id" value="{{$mapel->id}}">
                            <div class="col-md-12 mb-3">
                              <div>
                                <label for="nama_segment">Nama Segment</label>
                                <input name="nama_segment" class="form-control" id="nama_segment" type="text" placeholder="Nama Segment" value="{{$segment->nama_segment}}" required>
                              </div>
                            </div>
                          </div>
                          <button type="submit" class="btn btn-block btn-primary">Submit</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <tr>
              <td>
                <a href="{{ route('indexSegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id])}}">
                  <div class="d-flex align-items-center">
                    <div class="user-avatar bg-secondary mr-3"><i class="fas fa-box-open"></i></div>
                    <div class="d-block">
                      <span class="font-weight-bold">{{$segment->nama_segment}}</span>
                      <div class="small text-gray">{{ $segment->pertanyaans->count()}}  Pertanyaan</div>
                    </div>
                  </div>
                </a>
              </td>
              <td class="text-right">
                <div class="btn-group">
                  <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('copotSegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id])}}"><span class="fas fa-minus mr-2"></span>Copot Segment</a>
                    <a data-toggle="modal" data-target="#modal-edit-segment{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Edit Segment</a>
                    <a class="dropdown-item text-danger" href="{{ route('destroySegmentSoal', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $soal->id, $segment->id])}}"><span class="fas fa-user-times mr-2"></span>Hapus Permanent</a>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <div class="card-footer pb-3 border-0 d-flex align-items-center justify-content-between mt-5 pt-5">
        <nav aria-label="Page navigation example">
          <ul class="pagination mb-0">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav>
      </div>
    </div>
    
  </div>
  
  @include('backend/dir_materi/side')
</div>
@endsection