@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  <div class="modal fade" id="modal-upload-tugas" tabindex="-1" role="dialog" aria-labelledby="modal-upload-tugas" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body p-0">
          <div class="card border-light p-4">
            <div class="card-header text-center pb-0">
              <h2 class="mb-0 h5">Upload Tugas</h2>
              <p>Upload tugas dalam 1 file (pdf or zip)</p>
            </div>
            <div class="card-body">
              <form action="{{ route('uploadTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group my-4">
                  <label for="file">Tugas Anda</label>
                  <div class="file-field">
                    <div class="d-flex ml-xl-3">
                      <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                        <input type="file" id="file" name="file">
                        <div class="d-md-block text-left">
                          <div class="font-weight-normal text-dark mb-1">Pilih Dokumen</div>
                          <div class="text-gray small">PDF, PPT or DOC. Max size of 8MB</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary">Insert</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="col-12 col-xl-9">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h4 class="mt-4">{{ $tugas->nama_tugas }}</h4>
      <div class="mt-1">
        <h4 class="h6 font-weight-normal text-gray mb-3 mb-sm-0 small"><span class="fas fa-clock mr-2"></span>{{ date('Y-m-d, h:i A', strtotime($tugas->start)) }} - {{ date('Y-m-d, h:i A', strtotime($tugas->expired)) }}</h4>
      </div>
      <p class="card-text my-4">{!! nl2br($tugas->keterangan) !!}</p>
      <div class="row">
        <div class="col-md-6">
          <div class="d-flex align-items-center py-2">
            <a href="{{ route('downloadTugas', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id]) }}" class="animate-up-2">
              <div class="icon icon-shape icon-sm icon-shape-primary rounded mr-3"><span class="fas fa-download p-1"></span></div>
            </a>
            <div class="d-block"><label class="mb-0">Download Tugas</label></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="d-flex align-items-center py-2">
            @if($tugas->user_s()->where('user_id', Auth::user()->id )->exists())
            <span>
              <div class="icon icon-shape icon-sm icon-shape-success rounded mr-3"><span class="fas fa-download p-1"></span></div>
            </span>
            <div class="d-block text-success"><label class="mb-0">Sudah Mengerjakan</label></div>
            @else
            <a data-toggle="modal" data-target="#modal-upload-tugas" class="animate-up-2">
              <div class="icon icon-shape icon-sm icon-shape-primary rounded mr-3"><span class="fas fa-upload p-1"></span></div>
            </a>
            <div class="d-block"><label class="mb-0">Upload Tugas</label></div>
            @endif
          </div>
        </div>
      </div>
      
    </div>
  </div>
  @include('backend/dir_materi/side')
</div>
@endsection