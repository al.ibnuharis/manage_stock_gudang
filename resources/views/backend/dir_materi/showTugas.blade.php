@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  
  
  <div class="col-12 col-xl-9">
    <div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0">
      <table class="table user-table table-hover align-items-center">
        <thead>
          <tr>
            <th class="border-bottom py-4">Nama Siswa</th>
            <th class="border-bottom py-4">Tanggal</th>
            <th class="border-bottom py-4">Nilai</th>
            <th class="border-bottom py-4">Tugas</th>
            <th class="border-bottom py-4"></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($tugas->user_s as $siswa)
          <div class="modal fade" id="modal-input-nilai{{$loop->iteration}}" tabindex="-1" role="dialog" aria-labelledby="modal-input-nilai{{$loop->iteration}}" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-body p-0">
                  <div class="card border-light p-4">
                    <div class="card-body">
                      <form action="{{ route('nilaiTugasSiswa', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id, $siswa->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                          <div class="col-md-12 mb-3">
                            <div>
                              <label for="nilai">Input Nilai</label>
                              <input name="nilai" class="form-control" id="nilai" type="text" placeholder="Input Nilai" value="{{$siswa->pivot->nilai}}" required>
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <tr>
            <td>
              <div class="d-flex align-items-center">
                <div class="user-avatar bg-secondary mr-3"><span>SA</span></div>
                <div class="d-block">
                  <span class="font-weight-bold">{{ $siswa->name }}</span>
                  <div class="small text-gray">{{ $siswa->email }}</div>
                </div>
              </div>
            </td>
            <td><span class="font-weight-normal">{{ date('Y-m-d, h:i A', strtotime($siswa->pivot->tanggal)) }}</span></td>
            <td><span class="font-weight-normal">{{ $siswa->pivot->nilai }}</span></td>
            <td>
              <a href="{{ route('downloadTugasSiswa', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id, $siswa->id]) }}" class="scale-up-2">
                <div class="icon icon-shape icon-sm icon-shape-primary rounded mr-3"><span class="fas fa-download p-1"></span></div>
              </a>
            </td>
            <td>
              <div class="btn-group">
                <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                  <a data-toggle="modal" data-target="#modal-input-nilai{{$loop->iteration}}" class="dropdown-item"><span class="fas fa-user-shield mr-2"></span>Input Nilai</a>
                  <a class="dropdown-item text-danger" href="{{ route('destroyTugasSiswa', [$hari->slug, $kelas->id, $mapel->id, $materi->id, $tugas->id, $siswa->id]) }}"><span class="fas fa-user-times mr-2"></span>Retake</a>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="card-footer pb-3 border-0 d-flex align-items-center justify-content-between mt-5 pt-5">
        <nav aria-label="Page navigation example">
          <ul class="pagination mb-0">
            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav>
      </div>
    </div>
    
  </div>
  
  @include('backend/dir_materi/side')
</div>
@endsection