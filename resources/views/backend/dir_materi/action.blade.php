@extends('layouts.main')
@section('title', 'App_School')
@section('content')
<div class="row mt-4">
  <div class="col-12 col-xl-12">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h2 class="h5 mb-4">Materi Pelajaran Yang Di di Ajar</h2>
      <form method="POST" @if($action == 'create') action="{{ route('storeMateri', [$hari->slug, $kelas->id, $mapel->id]) }}" @else action="{{ route('updateMateri', [$hari->slug, $kelas->id, $mapel->id, $materi->id]) }}" @endif enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
        <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
        <input type="hidden" name="mapel_id" value="{{ $mapel->id }}">
        <div class="row">
          <div class="col-md-12 mb-3">
            <div>
              <label for="nama_materi">Judul Materi Pelajaran</label>
              <input name="nama_materi" class="form-control" id="nama_materi" type="text" placeholder="Judul Materi" @if($action == 'edit') value="{{ $materi->nama_materi }}" @endif required>
            </div>
          </div>
        </div>
        <div class="row align-items-center">
          <div class="col-md-3 mb-3">
            <label for="tanggal">Tanggal</label>
            <div class="input-group">
              <span class="input-group-text"><span class="far fa-calendar-alt"></span></span>
              <input name="tanggal" data-datepicker="" class="form-control" id="tanggal" type="text" placeholder="dd/mm/yyyy" @if($action == 'edit')  value="{{ date('m/d/Y', strtotime($materi->tanggal)) }}" @endif required>
            </div>
          </div>
          <div class="col-md-9 mb-3">
            <div class="form-group">
              <label for="pertemuan">Link Pertemuan (zoom, goggle meet, webex)</label>
              <input name="pertemuan" class="form-control" id="pertemuan" type="text" placeholder="Link Pertemuan Kelas" @if($action == 'edit') value="{{ $materi->pertemuan }}" @endif required>
            </div>
          </div>
        </div>
        <label for="file">Visual Banner</label>
        <div class="row align-items-center">
          <div class="col-sm-3 mb-3">
            <div class="form-group">
              <fieldset>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="banner" id="upload" value="1" onclick="funcResource()" @if($action == 'edit') @if($materi->banner == 1) checked @endif @endif>
                  <label class="form-check-label" for="upload">File Gambar</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="banner" id="link" value="0" onclick="funcResource()" @if($action == 'edit') @if($materi->banner == 0) checked @endif @endif>
                  <label class="form-check-label" for="link">Link Video Youtube</label>
                </div>
              </fieldset>
            </div>
          </div>
          <div class="col-sm-9 mb-3">
            <div class="form-group" @if($action == 'edit') @if($materi->banner == 1) style="display:block" @else style="display:none" @endif @else style="display:none" @endif id="UploadSystem">
              <div class="file-field">
                <div class="d-flex ml-xl-3">
                  <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                    <input type="file" id="file" name="file">
                    <div class="d-md-block text-left">
                      <div class="font-weight-normal text-dark mb-1">Pilih File Gamber</div>
                      <div class="text-gray small">Only JPG or PNG Max size of 2MB</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div @if($action == 'edit') @if($materi->banner == 0) style="display:block" @else style="display:none" @endif @else style="display:none" @endif class="row align-items-center" id="linkAddress">
              <div class="col-sm-12 mb-3">
                <div class="mb-3">
                  <input name="video" class="form-control" id="address" type="text" placeholder="Enter your link address" @if($action == 'edit') value="{{ $materi->video }}" @endif>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="my-4">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="keterangan" class="form-control" placeholder="Masukkan Deskripsi." id="deskripsi" rows="8">@if($action == 'edit') {{ $materi->keterangan }} @endif</textarea>
          </div>
        </div>

        <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
      </form>
    </div>
  </div>
</div>
@endsection