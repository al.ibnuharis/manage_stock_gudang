@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row mt-4">
  <div class="col-12 col-xl-12">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h2 class="h5 mb-4">Form Action</h2>
      
      <form @if($action == 'create') action="{{ route('storeKelas') }}" @else action="{{ route('updateKelas', $kelas->id) }}" @endif method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="2" name="role_id">
        <div class="row">
          <div class="col-md-12 mb-3">
            <div>
              <label for="name">Nama Kelas</label>
              <input name="nama_kelas" class="form-control" id="name" type="text" placeholder="Masukkan Nama Kelas" @if($action == 'edit') value="{{ $kelas->nama_kelas }}" @endif required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <div>
              <label for="state">Level Kelas</label>
              <select id="state" class="w-100" name="level_id">
                @foreach ($levels as $level)
                <option value="{{ $level->id }}" @if($action == 'edit') @if ($kelas->level_id == $level->id) selected @endif @endif>{{ $level->nama_level }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <div>
              <label for="state_2">Tahun Pelajaran</label>
              <select id="state_2" class="w-100" name="tahun_id">
                @foreach ($tahun_s as $tahun)
                <option value="{{ $tahun->id }}" @if($action == 'edit') @if ($kelas->tahun_id == $tahun->id) selected @endif @endif>{{ $tahun->tahun }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
      </form>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection