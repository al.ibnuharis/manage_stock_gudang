@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row mt-4">
  @foreach ($haris as $hari)
  <div class="col-sm-6 col-lg-4">
    <a href="{{ route('showHariKelas', [$kelas->id, $hari->id]) }}" class="page-preview scale-up-hover-2">
      <div class="card bg-white shadow-soft text-primary rounded mb-4">
        <div class="px-3 px-lg-4 py-5 text-center"><span class="icon icon-lg mb-4 scale"><span
              class="far fa-calendar-alt"></span></span>
          <h5 class="font-weight-bold text-primary">{{ $hari->nama_hari}}</h5>
          <p>6 Mata Pelajaran</p>
        </div>
      </div>
    </a>
  </div>
  @endforeach

</div>
{{-- konten --}}
@endsection