@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row my-4">
  <div class="col-12 col-xl-8 mb-4">
    <div class="row">
      <div class="col-12 mb-4">
        <div class="card border-light shadow-sm">
          <div class="card-body">
            <div class="row d-block d-xl-flex align-items-center">
              <div class="col-12 col-xl-2 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                <div class="icon icon-shape icon-md icon-shape-blue rounded mr-4 mr-sm-0 scale">
                  <span class="fas fa-inbox"></span>
                </div>
                <div class="d-sm-none">
                  <h2 class="h5">{{ $kelas->nama_kelas }}</h2>
                  <h3 class="mb-1">69 Siswa</h3>
                </div>
              </div>
              <div class="col-12 col-xl-10 px-xl-0">
                <div class="d-none d-sm-block">
                  <h2 class="h5">{{ $kelas->nama_kelas }}</h2>
                  <h3 class="mb-1">30 Siswa Dalam 1 Kelas</h3>
                </div>
                <small>9 Mataplajaran</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 mb-4">
        <div class="card border-light shadow-sm mb-4">
          <div class="card-body">
            <div class="row align-items-center">
              <div class="col text-left">
                <span class="icon icon-lg text-primary"><span class="fas fa-user-check"></span></span>
              </div>
              <div class="col-auto">
                <div class="btn-toolbar dropdown">
                  <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-plus mr-2"></span>New Task</button>
                  <div class="dropdown-menu dashboard-dropdown dropdown-menu-right mt-2">
                    <a class="dropdown-item font-weight-bold" href="{{ route('createSiswaKelas', $kelas->id)}}"><span class="fas fa-tasks"></span>Tambah Siswa</a> 
                    <a class="dropdown-item font-weight-bold" href=""><span class="fas fa-cloud-upload-alt"></span>Inport Siswa</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card border-light shadow-sm">
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Nama</th>
                  <th scope="col">Nis Siswa</th>
                  <th scope="col">Email</th>
                  <th scope="col">Gender</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($kelas->siswa_s as $siswa)
                <tr>
                  <th scope="row">{{ $siswa->name }}</th>
                  <td>{{ $siswa->nis_siswa }}</td>
                  <td>{{ $siswa->email }}</td>
                  <td>{{ $siswa->jenis_kelamin }}</td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item text-danger" href=""><span class="fas fa-user-times mr-2"></span>Inactive</a>
                        <a class="dropdown-item text-success" href=""><span class="fas fa-user-check mr-2"></span>Active</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <div class="col-12 col-xl-4 mb-4">

    <div class="card border-light shadow-sm mb-4">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-auto">
            <div class="icon icon-shape icon-md icon-shape-dark rounded mr-4 mr-sm-0 scale">
              <span class="fas fa-calendar-alt"></span>
            </div>
          </div>
          <div class="col text-right"><a href="{{ route('hariKelas', $kelas->id)}}" class="btn btn-sm btn-secondary"><span class="fas fa-cog mr-2"></span>Jadwal Kelas</a></div>
        </div>
      </div>
    </div>

  </div>
</div>

{{-- konten --}}
@endsection