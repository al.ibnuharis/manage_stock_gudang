@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="table-settings my-4">
  <div class="row">
    <div class="col-12">
      <a href="" class="btn btn-primary">Tambah Blog</a>
    </div>
  </div>
</div>
<div class="col-12 col-lg-6">
  <div class="card border-light p-2 p-md-4">
    <div class="card-body px-0">
      <a href="">
        <img src="https://1.bp.blogspot.com/-1a22K-l44fI/X3VRl2FCPVI/AAAAAAAAJLE/EDSPBNX8BT8SlASO-cKwv3zPA8Rm81MFwCLcBGAsYHQ/s16000/wallpaper-among-us-hd.jpg" class="card-img-top rounded" alt="blog image">
        <h4 class="my-4">List of public corporations by market capitalization</h4>
      </a>
      <p class="card-text mb-4">All of the world's 10 largest companies as measured by market capitalization
        are American. Market capitalization is the total value of a company's entire purchased shares of
        stock. While these companies...</p>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection