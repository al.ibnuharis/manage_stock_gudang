@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
  <div class="row my-4">
    <div class="col-12">
      <a href="{{ route('createGuru') }}" class="btn btn-primary">Tambah Guru</a>
    </div>
  </div>
<div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0 pb-4">
  <table class="table user-table table-hover align-items-center">
    <thead>
      <tr>
        <th class="border-bottom">Nama Guru</th>
        <th class="border-bottom">Date Created</th>
        <th class="border-bottom">Jumlah Ajar</th>
        <th class="border-bottom">Jumlah Mapel</th>
        <th class="border-bottom">Status</th>
        <th class="border-bottom"></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($guru_s as $guru)
      <tr>
        <td>
          <a href="#" class="d-flex align-items-center">
            @if($guru->foto !== NULL)
            <img src="{{ asset('storage/profiles/'. $guru->foto) }}" class="user-avatar rounded-circle mr-3" alt="Avatar">
            @else
            <div class="user-avatar bg-secondary mr-3"><span>?</span></div>
            @endif
            <div class="d-block">
              <span class="font-weight-bold">{{ $guru->name }}</span>
              <div class="small text-gray">{{ $guru->email }}</div>
            </div>
          </a>
        </td>

        <td><span class="font-weight-normal">{{ $guru->created_at->format("d M, h:i A") }}</span></td>
        <td><span class="font-weight-normal"> 6 Kelas<span></td>
        <td><span class="font-weight-normal"> MTK, IPA, BIG<span></td>
        @if($guru->is_active == 1)
        <td><span class="font-weight-normal text-success">Active<span></td>
        @else 
        <td><span class="font-weight-normal text-danger">Inactive<span></td>
        @endif
        <td>
          <div class="btn-group">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{ route('resetGuru', $guru->id) }}"><span class="fas fa-user-shield mr-2"></span>Reset Pass</a>
              <a class="dropdown-item" href="{{ route('editGuru', $guru->id) }}"><span class="fas fa-user-edit mr-2"></span>Edit Data</a>
              @if($guru->is_active == 1)
              <a class="dropdown-item text-danger" href="{{ route('inactiveGuru', $guru->id) }}"><span class="fas fa-user-times mr-2"></span>Inactive</a>
              @else 
              <a class="dropdown-item text-success" href="{{ route('activeGuru', $guru->id) }}"><span class="fas fa-user-check mr-2"></span>Active</a>
              @endif
              
            </div>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="card-footer pb-3 border-0 d-flex align-items-center justify-content-between mt-5 pt-5">
    <nav aria-label="Page navigation example">
      <ul class="pagination mb-0">
        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item active"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
      </ul>
    </nav>
  </div>
</div>
{{-- konten --}}
@endsection