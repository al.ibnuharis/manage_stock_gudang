@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="row mt-4">
  <div class="col-12 col-xl-12">
    <div class="card card-body bg-white border-light shadow-sm mb-4">
      <h2 class="h5 mb-4">Form Action</h2>
      
      <form @if($action == 'create') action="{{ route('storePengumuman') }}" @else action="{{ route('updatePengumuman', $pengumuman->id) }}" @endif method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-12 mb-3">
            <div>
              <label for="name_pengumuman">Nama Pengumuman</label>
              <input name="nama_pengumuman" class="form-control" id="name_pengumuman" type="text" placeholder="Nama Pengumuman" @if($action == 'edit') value="{{ $pengumuman->nama_pengumuman }}" @endif required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="waktu">Tanggal dan Waktu</label>
              <input name="waktu" class="form-control" id="waktu" type="datetime-local" @if($action == 'edit') value="{{ date('Y-m-d\TH:i', strtotime($pengumuman->waktu)) }}" @endif required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 mb-2">
            <div class="form-group">
              <label for="file">Banner</label>
              <div class="file-field">
                <div class="d-flex ml-xl-3">
                  <div class="d-flex"><span class="icon icon-md"><span class="fas fa-paperclip mr-3"></span></span>
                    <input type="file" id="file" name="file">
                    <div class="d-md-block text-left">
                      <div class="font-weight-normal text-dark mb-1">Choose Image</div>
                      <div class="text-gray small">JPG, PNG. Max size of 800K</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="my-4">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="keterangan" class="form-control" placeholder="Masukkan Deskripsi." id="deskripsi" rows="8">@if($action == 'edit') {{ $pengumuman->keterangan }} @endif</textarea>
          </div>
        </div>
        <div class="mt-3"><button type="submit" class="btn btn-primary">Save All</button></div>
      </form>
    </div>
  </div>
</div>
{{-- konten --}}
@endsection