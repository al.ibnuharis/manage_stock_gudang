@extends('layouts.main')
@section('title', 'App_School')
@section('content')
{{-- konten --}}
<div class="table-settings my-4">
  <div class="row">
    <div class="col-12">
      <a href="" class="btn btn-primary">Tambah Siswa</a>
    </div>
  </div>
</div>
<div class="card card-body border-light shadow-sm table-wrapper table-responsive pt-0">
  <table class="table user-table table-hover align-items-center">
    <thead>
      <tr>
        <th class="border-bottom">Nama Siswa</th>
        <th class="border-bottom">Nis Siswa</th>
        <th class="border-bottom">Kelas</th>
        <th class="border-bottom">Alamat</th>
        <th class="border-bottom">Status</th>
        <th class="border-bottom"></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($siswa_s as $siswa)
      <tr>
        <td>
          <a href="#" class="d-flex align-items-center">
            <div class="user-avatar bg-secondary mr-3"><span>SA</span></div>
            <div class="d-block">
              <span class="font-weight-bold">{{ $siswa->name }}</span>
              <div class="small text-gray">{{ $siswa->email }}</div>
            </div>
          </a>
        </td>
        <td><span class="font-weight-normal">{{ $siswa->nis_siswa }}</span></td>
        <td><span class="font-weight-normal">@if($siswa->kelas_s()->where('is_active', 1 )->exists()) {{ $siswa->kelas_s()->where('is_active', 1 )->first()->nama_kelas }} @endif<span></td>
        <td><span class="font-weight-normal">{{ $siswa->alamat }}<span></td>
        <td>@if($siswa->kelas_s()->where('is_active', 1 )->exists())<span class="font-weight-normal text-success"> Active<span>@else <span class="font-weight-normal text-danger"> InActive<span> @endif</td>
        <td>
          <div class="btn-group">
            <button class="btn btn-link text-dark dropdown-toggle dropdown-toggle-split m-0 p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="icon icon-sm"><span class="fas fa-ellipsis-h icon-dark"></span></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#"><span class="fas fa-user-shield mr-2"></span> Reset Pass</a>
              <a class="dropdown-item" href="#"><span class="fas fa-eye mr-2"></span>View Details</a>
              <a class="dropdown-item text-danger" href="#"><span class="fas fa-user-times mr-2"></span>Delete</a>
            </div>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="card-footer pb-3 border-0 d-flex align-items-center justify-content-between mt-5 pt-5">
    <nav aria-label="Page navigation example">
      <ul class="pagination mb-0">
        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item active"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
      </ul>
    </nav>
  </div>
</div>
{{-- konten --}}
@endsection