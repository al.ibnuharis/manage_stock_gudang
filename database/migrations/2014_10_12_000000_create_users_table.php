<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->foreignId('role_id')->nullable()->constrained('roles')->onDelete('cascade');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('nip_guru')->nullable();
            $table->string('nis_siswa')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->timestamp('tanggal_lahir')->nullable();
            $table->text('alamat')->nullable();
            $table->string('kota')->nullable();
            $table->string('nomor')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('pendidikan_guru')->nullable();
            $table->string('status')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('nik')->nullable();
            $table->string('agama')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->default(Hash::make('passdefault'));
            $table->text('foto')->nullable();
            $table->text('slug')->nullable();
            $table->boolean('is_active')->nullable()->default('1');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
